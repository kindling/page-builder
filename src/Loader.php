<?php

namespace Kindling\PageBuilder;

use Illuminate\Support\Str;

class Loader
{
    protected $key;

    /**
     * Constructor
     *
     * @param string $key
     */
    public function __construct($key = 'page_builder_modules')
    {
        $this->key = $key;
    }

    /**
     * Load the page builder.
     *
     * @return string
     */
    public function load()
    {
        return html_entity_decode($this->modules()->map(function ($module) {
            // Adds the ability to dd/dump all meta in views.
            if ($this->addDebug()) {
                $module->put('_page_builder_modules', $module->toArray(['_page_builder_names']));
                $module->put('_page_builder_names', $module->except(['_page_builder_modules'])->keys());
            }

            return view($this->path($module), $module);
        })->implode(''));
    }

    /**
     * Set if the loader should add the debug
     *
     * @return bool
     */
    public function addDebug()
    {
        return (bool) apply_filters('kindling_page_builder_add_debug', true);
    }

    /**
     * Get the view path.
     *
     * @param \Illuminate\Support\Collection $module
     * @return string
     */
    protected function path($module)
    {
        return apply_filters('kindling_page_builder_loader_view_path', "{$this->basePath()}.{$this->slug($module)}");
    }

    /**
     * Get a modules slug.
     *
     * @param \Illuminate\Support\Collection $module
     * @return string
     */
    protected function slug($module)
    {
        return Str::slug(trim(str_replace('_', '-', $this->layout($module)), '-'));
    }

    /**
     * Get a modules layout.
     *
     * @param \Illuminate\Support\Collection $module
     * @return string
     */
    protected function layout($module)
    {
        return $module->get('acf_fc_layout') ?: '';
    }

    /**
     * Get the modules.
     *
     * @return void
     */
    protected function modules()
    {
        $modules = get_field($this->key);
        $modules = is_array($modules) ? $modules : [];
        return r_collect($modules);
    }

    /**
     * Get the loader base path.
     *
     * @return string
     */
    protected function basePath()
    {
        $path = apply_filters('kindling_page_builder_loader_base_path', 'page-builder');
        $path = trim($path, '/');
        $path = trim($path, '.');

        return $path;
    }
}
