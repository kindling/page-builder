<?php
/**
 * Kindling Page Builder - Helpers
 *
 * @package Kindling_Page_Builder
 */

 /**
  * Kindling builder loader
  * Apply where you want the page builder to load.
  *
  * <code>
  * {!! kindling_page_builder_loader() !!}
  * </code>
  *
  * @return void
  */
function kindling_page_builder_loader()
{
    return (new \Kindling\PageBuilder\Loader)->load();
}
